require("tofik.set")
require("tofik.remap")
require("tofik.lazy")

-- background
-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
-- --vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
-- vim.api.nvim_set_hl(0, "NormalNC", { bg = "none" })


-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  group = vim.api.nvim_create_augroup('tofik-highlight-yank', { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

