vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.autoindent = true
vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.laststatus = 2
vim.opt.cmdheight = 0

-- Set windown title
vim.opt.title = true

-- Force clipboard not integrated with system one
vim.opt.clipboard = "unnamed"

-- disable swapfiles
vim.opt.swapfile = false

-- history settings {{{
vim.opt.backup = false
vim.opt.undodir = vim.fn.stdpath("data") .. "/undo"
vim.opt.undofile = true
-- }}}
-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 10

-- Show which line your cursor is on
vim.opt.cursorline = true
vim.opt.cursorcolumn = false
vim.opt.colorcolumn = "110"

vim.opt.signcolumn = "yes"

-- Decrease update time
vim.opt.updatetime = 50

vim.opt.isfname:append("@-@")


-- line breaking {{{
vim.opt.linebreak = false
-- Enable break indent
vim.opt.breakindent = true
-- }}}


-- Enable spell checker {{{
vim.opt.spelllang = {
  "en",
  "pl",
  "cs",
}
vim.opt.spell = true
-- }}}

-- Folding
vim.opt.foldmethod = 'marker'
