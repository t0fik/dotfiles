return {
  'ap/vim-css-color',
  'hashivim/vim-terraform',
  'martinda/Jenkinsfile-vim-syntax',
  'towolf/vim-helm',
  't0fik/salt-vim',
  -- Highlight todo, notes, etc in comments
  {
    'folke/todo-comments.nvim',
    event = 'VimEnter',
    dependencies = { 'nvim-lua/plenary.nvim' },
    opts = { signs = false }
  },
}
