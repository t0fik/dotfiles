alias vim='nvim'
alias vimdiff='nvim -d'
if (( $+commands[eza] )); then
	alias ls='eza'
elif (( $+commands[exa] )); then
	alias ls='exa'
fi

if (( $+commands[batcat] )); then
  alias cat='batcat -p'
  alias less='batcat -n'
else
  alias cat='bat -p'
  alias less='bat -n'
fi
