if (( ! $+command[kustomize] )); then
  return
fi

if [[ ! -f "$ZSH_CACHE_DIR/completions/_kustomize" ]]; then
  typeset -g -A _comps
  autoload -Uz _kustomize
  _comps[kustomize]=_kustomize
fi

kustomize completion zsh >| "$ZSH_CACHE_DIR/completions/_kustomize" &|
