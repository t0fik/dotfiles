if (( ! $+command[kubebuilder] )); then
  return
fi

if [[ ! -f "$ZSH_CACHE_DIR/completions/_kubebuilder" ]]; then
  typeset -g -A _comps
  autoload -Uz _kubebuilder
  _comps[kubebuilder]=_kubebuilder
fi

kubebuilder completion zsh >| "$ZSH_CACHE_DIR/completions/_kubebuilder" &|
