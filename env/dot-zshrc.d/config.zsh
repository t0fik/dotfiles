[[ ! -d "${XDG_CACHE_HOME}/zsh" ]] && mkdir -p "${XDG_CACHE_HOME}/zsh" 
[[ ! -d "${HISTFILE%/*}" ]] && mkdir -p "${HISTFILE%/*}" 

autoload -U compinit
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"
