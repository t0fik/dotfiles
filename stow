#!/usr/bin/env bash

set -euo pipefail

if [[ -f /etc/os-release ]];then
	set -o allexport
	source /etc/os-release
	set +o allexport
	VERSION_ID_NODOTS="$(printf "%s" "${VERSION_ID}"| sed 's/\.//g')"
	export VERSION_ID_NODOTS
fi

# Defaults
STOW_OPTS=(
	--restow
	--dotfiles
	--ignore='^(package|dependencies)$'
	)

XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-${HOME}/.config}

TRAPS=()

# Prerequisite rules for package
rules_ssh() {
	if [[ ! -d ${HOME}/.ssh ]]; then
		mkdir -p ${HOME}/.ssh/config.d
		chmod 700 ${HOME}/.ssh/
	fi
	return 0
}

rules_xfce4() {
	if [[ ! -d ${XDG_CONFIG_HOME}/xfce4 ]]; then
		mkdir -p ${XDG_CONFIG_HOME}/xfce4
	fi
	return 0
}

rules_scripts() {
	if [[ ! -d ${HOME}/.local/bin ]]; then
		mkdir -p ${HOME}/.local/bin
	fi
	return 0
}

rules_i3(){
	sudo dnf -y copr enable atim/i3status-rust
	sudo dnf -y install \
		i3status-rust \
		picom \
		dunst \
		fontawesome-fonts-all \
		terminus-fonts \
		j4-dmenu-desktop \
		bemenu
	return 0
}


# Dummy rule
no_rules() {
	return 0
}

get_rules() {
  if [[ "$(type -t "rules_$1")" = "function" ]]; then
    printf "rules_$1"
  else
    printf "no_rules"
  fi
}

DEPENDENCIES=()
resolve_dependencies(){
	local -a deps
	local dep
	if [[ -f "${1}/dependencies" ]];then
		mapfile -t deps < "${1}/dependencies"
		for dep in "${deps[@]}"; do
			if printf "%s\0" "${DEPENDENCIES[@]}" | grep -Fxqz -- "${dep}" || [[ "${1}" = "${dep}" ]]; then
				continue
			fi
			printf "%s\n" "${dep}"
			DEPENDENCIES+=( "${dep}" )
			resolve_dependencies "${dep}"
		done
	fi
	return 0
}

cleanup() {
	for tr in "${TRAPS[@]}"; do
		${tr}
	done
}

trap cleanup EXIT

main() {
	local -a packages
	local package
	local rules

	mapfile -t packages < <(resolve_dependencies "${1}")
	packages+=( "${1}" )
	for package in "${packages[@]}"; do
		if [[ -f "${package}/package" ]]; then
			source "${package}/package"
		fi
		rules=$(get_rules "${package}")
		${rules}
		stow "${STOW_OPTS[@]}" "${package}"
	done


}


if ! which stow >/dev/null 2>&1; then
	printf "'stow' command not available\n" >&2
	exit 1
fi

main "${1}"
